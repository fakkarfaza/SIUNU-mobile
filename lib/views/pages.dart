import 'dart:convert';

// Package
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:another_flutter_splash_screen/another_flutter_splash_screen.dart';
import 'package:ptnu_mobile_app/main.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_charts/sparkcharts.dart';
import 'package:url_launcher/url_launcher.dart';

// Komponen 
import 'package:ptnu_mobile_app/api/auth_service.dart';
import 'package:ptnu_mobile_app/utils/inter_font.dart';
import 'package:ptnu_mobile_app/views/widgets.dart';

// Halaman
part 'pages/akademik/akademik_dashboard.dart';
part 'pages/akademik/akademik_dosen.dart';
part 'pages/akademik/akademik_kalender.dart';
part 'pages/akademik/akademik_kelulusan.dart';
part 'pages/akademik/akademik_mahasiswa.dart';
part 'pages/akademik/akademik_nilai.dart';
part 'pages/akademik/akademik_tracer_study.dart';

part 'pages/ppm/ppm_dashboard.dart';
part 'pages/ppm/ppm_penelitian.dart';
part 'pages/ppm/ppm_hki.dart';
part 'pages/ppm/ppm_hibah.dart';






part 'pages/mbkm/mbkm_dashboard.dart';
part 'pages/operasional/operasional_dashboard.dart';
part 'pages/laporan/laporan_dashboard.dart';
part 'pages/notifikasi_page.dart';
part 'pages/persuratan/persuratan_dashboard.dart';
part 'pages/dashboard_page.dart';
part 'pages/login_page.dart';
part 'pages/pmb_page.dart';
part 'pages/portal_page.dart';
part 'pages/splash_page.dart';
