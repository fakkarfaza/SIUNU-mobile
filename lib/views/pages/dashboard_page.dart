part of '../pages.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {

  final storage = const FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return  Scaffold (
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(color: Colors.green, height: 340),
            Padding(
              padding: const EdgeInsets.only(top: 44, left:28, right: 28),
              child: Column(
                children: [
                  Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // profile picture
                      const CircleAvatar(
                        radius: 17, 
                        backgroundColor: Colors.white,
                        backgroundImage: AssetImage('assets/images/profile_picture.png'),
                      ),
                      // name
                      Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: InterFont.custom(text: 'John Doe' ,fontSize: 12 ,fontWeight: FontWeight.w700, color: Colors.white),
                      ),
                      const Spacer(),
                      // Iconbutton
                      IconButton(
                        icon: const Icon(Icons.notifications_none), 
                        onPressed: (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => const NotifikasiPage()));}, 
                        color: Colors.white
                      ),
                      IconButton(
                        icon: const Icon(Icons.logout_outlined),
                        onPressed: (){
                          storage.delete(key: 'token');
                          print(storage.read(key: 'token'));
                          Navigator.pushAndRemoveUntil(
                            context, 
                            MaterialPageRoute(
                              builder: (BuildContext context) => PortalPage(),
                            ),
                            (_) => false,
                          );
                        },
                        color: Colors.white,
                      )
                    ]
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0 , 20),
                    child: Center(
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/images/logo_siunu_white.png',
                            scale: 0.75,
                          ),
                          const SizedBox(height: 10),
                          InterFont.custom(
                            text: 'SIUNU',
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                          ), 
                          const SizedBox(height: 10),
                          InterFont.custom(
                            text: 'Sistem Integrasi Universitas Nahdlatul Ulama',
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                  DashboardSwipesIcons(),
                  Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InterFont.custom(text: 'Berita Terbaru', fontSize: 14, fontWeight: FontWeight.w800, color: Colors.black),
                          TextButton(
                            onPressed: () async { 
                              final url = 'https://lptnu.or.id/category/berita/';
                              await launchUrl(Uri.parse(url));
                            }, 
                            child: 
                              InterFont.custom(text: 'Lihat Semua', fontSize: 12, fontWeight: FontWeight.w600, color: Color(0xFF28C76F)), 
                          )
                        ],
                      ),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 24),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              NewsCard(description: 'Dampak perkembangan teknologi AI pada budaya dan peradaban manusia', imagePath: 'assets/images/dampak_perkembangan_ai_image.jpeg', imageUrl: 'https://lptnu.or.id/2024/02/09/ketua-umum-pbnu-gus-yahya-apresiasi-langkah-menarik-universitas-nahdlatul-ulama-pasuruan/'),
                              NewsCard(description: 'Ketua Umum PBNU, Gus Yahya, Apresiasi Langkah Menarik Universitas Nahdlatul Ulama Pasuruan', imagePath: 'assets/images/ketua_umum_image.jpeg', imageUrl: 'https://lptnu.or.id/2024/02/09/ketua-umum-pbnu-gus-yahya-apresiasi-langkah-menarik-universitas-nahdlatul-ulama-pasuruan/'),
                              NewsCard(description: 'LPTNU Sahkan Kepengurusan AFEBNU', imagePath: 'assets/images/lptnu_sahkan_image.png', imageUrl: 'https://lptnu.or.id/2024/01/26/lptnu-sahkan-kepengurusan-afebnu/'),
                            ],
                          ),
                        ),
                      ),
                      Padding( 
                        padding: const EdgeInsets.symmetric(vertical: 19),
                        child:
                        InterFont.custom(text: 'SISTEM INTEGRASI UNIVERSITAS NAHDLATUL ULAMA 2024', fontSize: 8, fontWeight: FontWeight.w400, color: Colors.black)
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

