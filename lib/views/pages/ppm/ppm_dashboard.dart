part of '../../pages.dart';

class PpmDashboard extends StatelessWidget {
  const PpmDashboard({super.key});

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF28C76F),
        title: InterFont.custom(text: 'Dashboard', fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white),
        centerTitle: true,
        iconTheme: const IconThemeData(color: Colors.white),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.home),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardPage()));
            },
          ),
        ],
      ),
      drawer: PPMDrawer(),
      body: const Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    vertical: 12, horizontal: 16),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          StatisticCard(
                              data: '2',
                              title: 'Penelitian',
                              size: 155,
                              icon: Icons.people_outline,
                              iconColor: Colors.green),
                          StatisticCard(
                              data: '1',
                              title: 'Pengabdian Masyakarat',
                              size: 155,
                              icon: Icons.people_outline,
                              iconColor: Colors.green),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          StatisticCard(
                              data: '2',
                              title: 'HKI (Hak Kekayaan Intelektual)',
                              size: 155,
                              icon: Icons.people_outline,
                              iconColor: Colors.green),
                          StatisticCard(
                              data: '2',
                              title: 'Hibah',
                              size: 155,
                              icon: Icons.people_outline,
                              iconColor: Colors.green),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Footer()
        ],
      ),
    );
  }
}