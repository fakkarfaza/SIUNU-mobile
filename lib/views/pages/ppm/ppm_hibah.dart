part of '../../pages.dart';

class PpmHibah extends StatelessWidget {
  const PpmHibah({super.key});

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF28C76F),
        title: InterFont.custom(text: 'Dashboard', fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white),
        centerTitle: true,
        iconTheme: const IconThemeData(color: Colors.white),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.home),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardPage()));
            },
          ),
        ],
      ),
      drawer: PPMDrawer(),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                vertical: 12, horizontal: 16),
                child: Column(
                  children: [
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          StatisticCard(data: 'Rp 300.000,00', title: 'Total Hibah', size: 310, icon: Icons.people_outline, iconColor: Colors.green),
                        ],
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          StatisticCard(data: 'Rp 100.000,00', title: 'Hibah Tahun Ini', size: 310, icon: Icons.people_outline, iconColor: Colors.green),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16),
                      child: Column(
                        children: [
                          InterFont.custom(text: 'Fluktuasi Total Hibah', fontSize: 18, fontWeight: FontWeight.w500, color: Color.fromARGB(255, 100, 100, 100), overflow: TextOverflow.clip),
                          InterFont.custom(text: 'dalam Rentang Tahun ', fontSize: 12, fontWeight: FontWeight.w400, color: Color(0xFF2B2D3DB6)),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 12),
                      child: Container(
                        height: 300, 
                        child: 
                        SfCartesianChart(
                          primaryXAxis: const CategoryAxis(
                            majorGridLines: MajorGridLines(width: 0),
                            majorTickLines: MajorTickLines(width: 0),
                          ),
                          primaryYAxis: const NumericAxis(
                            majorTickLines: MajorTickLines(width: 0),
                            majorGridLines: MajorGridLines(width: 0),
                          ),
                          series: <CartesianSeries>[
                            LineSeries<ChartSampleData, String>(
                              color: const Color(0xFF28C76F),
                              dataSource: chartData,
                              dataLabelSettings: const DataLabelSettings(isVisible: true),
                              xValueMapper: (ChartSampleData sales, _) => sales.x,
                              yValueMapper: (ChartSampleData sales, _) => sales.y
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Footer()
        ],
      ),
    );
  }
}