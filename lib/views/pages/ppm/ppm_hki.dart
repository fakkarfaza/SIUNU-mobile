part of '../../pages.dart';

class PpmHki extends StatelessWidget {
  const PpmHki({super.key});

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF28C76F),
        title: InterFont.custom(text: 'Hak Kekayaan Intelektual', fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white),
        centerTitle: true,
        iconTheme: const IconThemeData(color: Colors.white),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.home),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardPage()));
            },
          ),
        ],
      ),
      drawer: PPMDrawer(),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                vertical: 12, horizontal: 16),
                child: Column(
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16),
                        child: Column(
                          children: [
                            InterFont.custom(text: 'Rekapitulasi HKI', fontSize: 18, fontWeight: FontWeight.w500, color: Color.fromARGB(255, 100, 100, 100)),
                          ],
                        ),
                      ),
                    InterFont.custom(text: 'Grafik HKI Per Kampus', fontSize: 12, fontWeight: FontWeight.w600, color: Color.fromARGB(255, 100, 100, 100)),
                    Padding(
                      padding: const EdgeInsets.only(top: 12),
                      child: Container(
                        height: 300, 
                        child: 
                        SfCartesianChart(
                          primaryXAxis: const CategoryAxis(
                            majorGridLines: MajorGridLines(width: 0),
                            majorTickLines: MajorTickLines(width: 0),
                          ),
                          primaryYAxis: const NumericAxis(
                            majorTickLines: MajorTickLines(width: 0),
                            majorGridLines: MajorGridLines(width: 0),
                          ),
                          series: <CartesianSeries>[
                            ColumnSeries<ChartSampleData, String>(
                              color: const Color(0xFF28C76F),
                              borderRadius: const BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25)),
                              dataSource: chartData,
                              dataLabelSettings: const DataLabelSettings(isVisible: true),
                              xValueMapper: (ChartSampleData sales, _) => sales.x,
                              yValueMapper: (ChartSampleData sales, _) => sales.y
                            ),
                          ],
                        ),
                      ),
                    ),
                    InterFont.custom(text: 'Grafik HKI Per Tahun', fontSize: 12, fontWeight: FontWeight.w600, color: Color.fromARGB(255, 100, 100, 100)),
                    Padding(
                      padding: const EdgeInsets.only(top: 12),
                      child: Container(
                        height: 300, 
                        child: 
                        SfCartesianChart(
                          primaryXAxis: const CategoryAxis(
                            majorGridLines: MajorGridLines(width: 0),
                            majorTickLines: MajorTickLines(width: 0),
                          ),
                          primaryYAxis: const NumericAxis(
                            majorTickLines: MajorTickLines(width: 0),
                            majorGridLines: MajorGridLines(width: 0),
                          ),
                          series: <CartesianSeries>[
                            ColumnSeries<ChartSampleData, String>(
                              color: const Color(0xFF28C76F),
                              borderRadius: const BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25)),
                              dataSource: chartData,
                              dataLabelSettings: const DataLabelSettings(isVisible: true),
                              xValueMapper: (ChartSampleData sales, _) => sales.x,
                              yValueMapper: (ChartSampleData sales, _) => sales.y
                            ),
                          ],
                        ),
                      ),
                    ),
                    InterFont.custom(text: 'Total HKI Per Fakultas', fontSize: 12, fontWeight: FontWeight.w600, color: Color.fromARGB(255, 100, 100, 100)),
                    Padding(
                      padding: const EdgeInsets.only(top: 12),
                      child: 
                      Container(
                        height: 400,
                        child: Container(
                        height: 300, 
                        child: 
                        SfCartesianChart(
                          primaryXAxis: CategoryAxis(),
                          primaryYAxis: CategoryAxis(),
                          series: <CartesianSeries>[
                            BarSeries<ChartSampleData, String>(
                              width: 0.5,
                              color: const Color(0xFF28C76F),
                              borderRadius: const BorderRadius.only(bottomRight: Radius.circular(25), topRight: Radius.circular(25)),
                              dataSource: chartData,
                              dataLabelSettings: const DataLabelSettings(isVisible: true),
                              xValueMapper: (ChartSampleData sales, _) => sales.x,
                              yValueMapper: (ChartSampleData sales, _) => sales.y,
                            ),
                          ],
                        ),
                      ),
                      ),
                    ),
                    InterFont.custom(text: 'Total HKI Per Program Studi', fontSize: 12, fontWeight: FontWeight.w600, color: Color.fromARGB(255, 100, 100, 100)),
                    Padding(
                      padding: const EdgeInsets.only(top: 12),
                      child: 
                      Container(
                        height: 400,
                        child: Container(
                        height: 300, 
                        child: 
                        SfCartesianChart(
                          primaryXAxis: CategoryAxis(),
                          primaryYAxis: CategoryAxis(),
                          series: <CartesianSeries>[
                            BarSeries<ChartSampleData, String>(
                              width: 0.5,
                              color: const Color(0xFF28C76F),
                              borderRadius: const BorderRadius.only(bottomRight: Radius.circular(25), topRight: Radius.circular(25)),
                              dataSource: chartData,
                              dataLabelSettings: const DataLabelSettings(isVisible: true),
                              xValueMapper: (ChartSampleData sales, _) => sales.x,
                              yValueMapper: (ChartSampleData sales, _) => sales.y,
                            ),
                          ],
                        ),
                      ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Footer()
        ],
      ),
    );
  }
}


List<SalesData> getChartData() {
  final List<SalesData> chartData = [
    SalesData( 'Monday',1400),
    SalesData('Tuesday',600 ),
    SalesData( 'Wednesday',300),
    SalesData( 'Thursday', 600),
    SalesData(  'Friday',100),
  ];

  return chartData;
}

class SalesData {
final daysOfWeek;
final sale;

SalesData(  this.daysOfWeek, this.sale);
}