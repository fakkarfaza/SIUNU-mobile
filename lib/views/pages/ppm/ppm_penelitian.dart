part of '../../pages.dart';

class PpmPenelitian extends StatelessWidget {
  const PpmPenelitian({super.key});

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF28C76F),
        title: InterFont.custom(text: 'Penelitian', fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white),
        centerTitle: true,
        iconTheme: const IconThemeData(color: Colors.white),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.home),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardPage()));
            },
          ),
        ],
      ),
      drawer: PPMDrawer(),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                vertical: 12, horizontal: 16),
                child: Column(
                  children: [
                    Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Column(
                          children: [
                            InterFont.custom(text: 'Peningkatan Jumlah Penelitian', fontSize: 18, fontWeight: FontWeight.w500, color: Color.fromARGB(255, 100, 100, 100)),
                          ],
                        ),
                      ),
                      Padding(
                      padding: const EdgeInsets.only(top: 12),
                      child: Container(
                        height: 300, 
                        child: 
                        SfCartesianChart(
                          primaryXAxis: const CategoryAxis(
                            majorGridLines: MajorGridLines(width: 0),
                            majorTickLines: MajorTickLines(width: 0),
                          ),
                          primaryYAxis: const NumericAxis(
                            majorTickLines: MajorTickLines(width: 0),
                            majorGridLines: MajorGridLines(width: 0),
                          ),
                          series: <CartesianSeries>[
                            ColumnSeries<ChartSampleData, String>(
                              color: const Color(0xFF28C76F),
                              borderRadius: const BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25)),
                              dataSource: chartData,
                              dataLabelSettings: const DataLabelSettings(isVisible: true),
                              xValueMapper: (ChartSampleData sales, _) => sales.x,
                              yValueMapper: (ChartSampleData sales, _) => sales.y
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Footer()
        ],
      ),
    );
  }
}