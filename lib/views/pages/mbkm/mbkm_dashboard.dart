part of '../../pages.dart';

class MbkmDashboard extends StatelessWidget {
  const MbkmDashboard({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF28C76F),
        title: InterFont.custom(text: 'MBKM', fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
      ),
    );
  }
}