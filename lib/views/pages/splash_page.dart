part of '../pages.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    return FlutterSplashScreen.fadeIn(
      backgroundImage: Image.asset('assets/images/background.png'),
      childWidget: SizedBox(
        height: 200,
        child: Image.asset('assets/images/logo_siunu.png'),
      ),
      duration: const Duration(milliseconds: 1500),
      animationDuration: const Duration(milliseconds: 1000),
      nextScreen: CheckAuth(),
    );
  }
}