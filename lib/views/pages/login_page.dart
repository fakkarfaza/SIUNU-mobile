part of '../pages.dart';


class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  
  final Map<String, dynamic> dataLogin = {};
  final numberController = TextEditingController();
  final passwordController = TextEditingController();
  final textFieldFocusNode = FocusNode();
  bool _obscured = true;
  bool _isLoading = false;

  String? token = '';


  void _toggleObscured() {
    setState(() {
      _obscured = !_obscured;
      if (textFieldFocusNode.hasPrimaryFocus) return; 
      textFieldFocusNode.canRequestFocus = false;     
    });
  }
  
  Future<void> signIn(BuildContext context) async {

    setState(() {
      _isLoading = true;
    });

    final number = numberController.text;
    final password = passwordController.text;
    final storage = FlutterSecureStorage();

    try {
      http.Response response = await AuthService.signIn(number, password);
      Map<String, dynamic> dataStatus = json.decode(response.body);

      if (response.statusCode == 200) { // Login sukses

        await storage.write(key: "token", value: dataStatus['token']);

        token = await storage.read(key: 'token');

        Navigator.pushAndRemoveUntil(
          context, 
          MaterialPageRoute(
            builder: (BuildContext context) => DashboardPage(),
          ),
          (_) => false,
        );

      } else { // Login gagal
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Login failed')));
      }
    } catch (e) { // Error
      print('$e');
      String errorMessage = e.toString().replaceAll('Exception: ', '');;
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(errorMessage)));
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold (
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(color: Color(0xFF28C76F), height: 250),
            Container(color: Colors.white.withOpacity(0.8), height: 250),
            Center(
              child: 
                Padding(
                  padding: const EdgeInsets.only(top: 24),
                  child: Column(
                    children: [
                      SizedBox(height: 9),
                      Center(
                        child:
                        Image.asset(
                          'assets/images/logo_siunu.png',
                          height: 185,
                          width: 153,
                        ),
                      ),
                      SizedBox(height: 60),
                      InterFont.custom(text: 'Selamat Datang', fontSize: 22, fontWeight: FontWeight.bold, color: Color(0xFF4B465C)),
                      SizedBox(height: 24),
                      MyTextField(
                        controller: numberController,
                        hintText: 'No. Telp',
                        obscureText: false,
                      ),
                      SizedBox(height: 16),
                      Padding( 
                        padding: const EdgeInsets.symmetric(horizontal: 25),
                        child: SizedBox(
                          height: 48,
                          child: TextField(
                            controller: passwordController,
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: _obscured,
                            focusNode: textFieldFocusNode,
                            decoration: InputDecoration( //////////////////Input Decor
                              enabledBorder:  OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey.shade400),
                                borderRadius: BorderRadius.circular(12),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey.shade600),
                                borderRadius: BorderRadius.circular(12),
                              ),
                              fillColor: Colors.white.withOpacity(0.1),
                              filled: true,
                              hintText: 'Password',
                              hintStyle: GoogleFonts.inter(
                                color: Color(0xFF8F9098),
                                fontSize: 14,
                              ),
                              suffixIcon: Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 4, 0),
                                child: GestureDetector(
                                  onTap: _toggleObscured,
                                  child: Icon(
                                    _obscured
                                      ? Icons.visibility_outlined
                                      : Icons.visibility_off_outlined,
                                    size: 24,
                                  )
                                ),
                              )
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Lupa Password',
                            style: TextStyle(
                              color: Colors.green[500],
                              fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 24),
                      _isLoading
                        ? CircularProgressIndicator() // Tampilkan CircularProgressIndicator jika isLoading true
                        : MyButton(
                            onTap: () => signIn(context),
                          ),
                      SizedBox(height: 16),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   children: [
                      //     InterFont.custom(text: 'Belum punya akun? ', fontSize: 12, fontWeight: FontWeight.w400, color: Colors.black),
                      //     GestureDetector(
                      //       onTap: _launchInformasiYST,
                      //       child: InterFont.custom(text: 'Daftar', fontSize: 12, fontWeight: FontWeight.w400, color: Colors.green)),
                      //   ],
                      // )
                    ],
                  ),
                ),
            ),
          ],
        ),
      ),
    );
  }
}