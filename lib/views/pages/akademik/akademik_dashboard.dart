part of '../../pages.dart';

class AkademikDashboard extends StatelessWidget {
  const AkademikDashboard ({super.key});

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF28C76F),
        title: InterFont.custom(text: 'Dashboard', fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white),
        centerTitle: true,
        iconTheme: const IconThemeData(color: Colors.white),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.home),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardPage()));
            },
          ),
        ],
      ),
      drawer: AkademikDrawer(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical:12, horizontal: 16),
          child: Column(
            children: [
              const Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    StatisticCard(data: '4', title: 'Perguruan Tinggi', size: 155, icon: Icons.people_outline, iconColor: Colors.green),
                    StatisticCard(data: '14', title: 'Program Studi', size: 155, icon: Icons.people_outline, iconColor: Colors.green),
                  ],
                ),
              ),
              const Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    StatisticCard(data: '86', title: 'Dosen', size: 155, icon: Icons.people_outline, iconColor: Colors.green),
                    StatisticCard(data: '3427', title: 'Mahasiswa Aktif', size: 155, icon: Icons.people_outline, iconColor: Colors.green),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 6),
                child: Container(
                  height: 200,
                  child: Maps(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 19),
                child: InterFont.custom(text: 'SISTEM INTEGRASI UNIVERSITAS NAHDLATUL ULAMA 2024', fontSize: 8, fontWeight: FontWeight.w400, color: Colors.black)
            ),
            ],
          ),
        ),
      ),
    );
  }
}

class ChartSampleData {
  ChartSampleData(this.x, this.y);
  final String? x;
  final double? y;
}

final List<ChartSampleData> chartData = [
  ChartSampleData('jan', 50.56),
  ChartSampleData('Feb', 49.42),
  ChartSampleData('Mar', 53.21),
  ChartSampleData('Apr', 64.78),
  ChartSampleData('May', 100),
];