part of '../../pages.dart';

class AkademikMahasiswa extends StatelessWidget {
  const AkademikMahasiswa({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF28C76F),
        title: InterFont.custom(text: 'Mahasiswa', fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.home),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardPage()));
            },
          ),
        ],
      ),
      drawer: AkademikDrawer(),
      body:  SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      StatisticCard(data: '4', title: 'Total Mahasiswa Aktif ', size: 310, icon: Icons.people_outline, iconColor: Colors.green),
                    ],
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      StatisticCard(data: '4', title: 'Total Mahasiswa Tidak Aktif ', size: 310, icon: Icons.people_outline, iconColor: Colors.green),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Column(
                    children: [
                      InterFont.custom(text: 'Peningkatan Jumlah Mahasiswa', fontSize: 18, fontWeight: FontWeight.w500, color: Color.fromARGB(255, 100, 100, 100)),
                      InterFont.custom(text: 'Per Tahun', fontSize: 12, fontWeight: FontWeight.w400, color: Color(0xFF2B2D3DB6)),
                    ],
                  ),
                ),
                Padding(
                padding: const EdgeInsets.only(top: 12),
                child: Container(
                  height: 300, 
                  child: 
                  SfCartesianChart(
                    primaryXAxis: const CategoryAxis(
                      majorGridLines: MajorGridLines(width: 0),
                      majorTickLines: MajorTickLines(width: 0),
                    ),
                    primaryYAxis: const NumericAxis(
                      majorTickLines: MajorTickLines(width: 0),
                      majorGridLines: MajorGridLines(width: 0),
                    ),
                    series: <CartesianSeries>[
                      ColumnSeries<ChartSampleData, String>(
                        color: const Color(0xFF28C76F),
                        borderRadius: const BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25)),
                        dataSource: chartData,
                        dataLabelSettings: const DataLabelSettings(isVisible: true),
                        xValueMapper: (ChartSampleData sales, _) => sales.x,
                        yValueMapper: (ChartSampleData sales, _) => sales.y
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Column(
                    children: [
                      InterFont.custom(text: 'Rekap Status Mahasiswa', fontSize: 18, fontWeight: FontWeight.w500, color: Color.fromARGB(255, 100, 100, 100)),
                      InterFont.custom(text: 'Per Tahun', fontSize: 12, fontWeight: FontWeight.w400, color: Color(0xFF2B2D3DB6)),
                    ],
                  ),
                ),
              // Insert Pie Chart
              Padding(
                  padding: const EdgeInsets.only(top: 12),
                  child: Container(
                    height: 300, 
                    child: SfCircularChart(
                      series: <CircularSeries>[
                        PieSeries<ChartSampleData, String>(
                          dataSource: chartData,
                          xValueMapper: (ChartSampleData data, _) => data.x,
                          yValueMapper: (ChartSampleData data, _) => data.y,
                          dataLabelSettings: const DataLabelSettings(isVisible: true),
                        ),
                      ],
                      borderColor:  Color(0xFF2B2D3DB6), 
                      borderWidth: 1, 
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      )
    );
  }
}

