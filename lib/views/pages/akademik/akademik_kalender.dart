part of '../../pages.dart';

class AkademikKalender extends StatelessWidget {
  const AkademikKalender({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF28C76F),
        title: InterFont.custom(text: 'Kalender Akademik', fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.home),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardPage()));
            },
          ),
        ],
      ),
      drawer: AkademikDrawer(),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
        ),
      ),
    );
  }
}