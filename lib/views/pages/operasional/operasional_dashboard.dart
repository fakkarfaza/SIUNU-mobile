part of '../../pages.dart';

class OperasionalDashboard extends StatelessWidget {
  const OperasionalDashboard({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF28C76F),
        title: InterFont.custom(text: 'Operasional', fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
      ),
    );
  }
}