part of '../widgets.dart';

class Footer extends StatelessWidget {
  const Footer({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding( 
      padding: const EdgeInsets.symmetric(vertical: 19),
      child: InterFont.custom(text: 'SISTEM INTEGRASI UNIVERSITAS NAHDLATUL ULAMA 2024', fontSize: 8, fontWeight: FontWeight.w400, color: Colors.black)
    );
  }
}