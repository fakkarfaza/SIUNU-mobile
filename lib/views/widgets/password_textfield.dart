part of '../widgets.dart';

class PasswordTextField extends StatelessWidget {
  final controller;
  final String hintText;
  final bool obscureText;
  
  
  const PasswordTextField ({
    super.key,
    required this.controller,
    required this.hintText,
    required this.obscureText,
  });

@override
Widget build(BuildContext context){
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 25.0),
    child: SizedBox(
      height: 48,
      child: TextField(
        controller: controller,
        obscureText: obscureText,
        decoration: InputDecoration(
          enabledBorder:  OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade400),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade600),
            borderRadius: BorderRadius.circular(12),
          ),
          fillColor: Colors.white.withOpacity(0.1),
          filled: true,
          hintText: hintText,
          hintStyle: GoogleFonts.inter(
            color: Color(0xFF8F9098),
            fontSize: 14,
          )
        )),
    ),
    );
}
}