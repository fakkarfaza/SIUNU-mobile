part of '../widgets.dart';


class DashboardSwipesIcons extends StatefulWidget {

  @override
  State<DashboardSwipesIcons> createState() => _DashboardSwipesIconsState();

  DashboardSwipesIcons({Key? key}) : super(key: key);
}

class _DashboardSwipesIconsState extends State<DashboardSwipesIcons> {

  int _current = 0;
  final CarouselController _controller = CarouselController(); 

  final arrayOfIcons = [
    const Padding(
      padding: EdgeInsets.symmetric(horizontal: 12),
      child:  Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              IconWidget(icon: Icons.school_outlined, title: 'Akademik', page: AkademikDashboard()), 
              IconWidget(icon: Icons.person_search_outlined, title: 'PPM', page: PpmDashboard()),
            ],
          ),
          SizedBox(height: 6),
          Row(
           children: [
              IconWidget(icon: Icons.settings_outlined, title: 'Operasional', page: OperasionalDashboard()),
              IconWidget(icon: Icons.school, title: 'MBKM', page: MbkmDashboard()),
            ],
          ),
        ],
      ),
    ),
    const Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              IconWidget(icon: Icons.content_paste_outlined, title: 'Laporan', page: LaporanDashboard()),
              IconWidget(icon: 'assets/images/cashplus_icon.png', title: 'Cashplus', page:  Browser(url: 'http://cashplus.id/')),
            ],
          ),
          SizedBox(height: 6),
          Row(
            children: [
              IconWidget(icon: Icons.email_outlined, title: 'Persuratan', page: PersuratanDashboard()),
              IconWidget(icon: 'assets/images/simakpay_icon.png', title: 'SimakPay', page: Browser(url: 'https://app.simakpay.id/')),
            ],
          ),
        ],
      ),
    ];

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      // height: MediaQuery.of(context).size.height * 0.3,
      height: 180,
      width: MediaQuery.of(context).size.height * 0.8,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3),
          )
        ],
      ),
      child: 
      CarouselSlider(
        carouselController: _controller,
        options: CarouselOptions(
          height: 400.0,
          viewportFraction: 1,
          enableInfiniteScroll: false,
          onPageChanged: (index, reason) {
            setState(() {
              _current = index;
            });
          }
        ),
        items: arrayOfIcons.map((itemSlider) {
          return Builder(
            builder: (BuildContext context) {
              return 
              Padding(
                padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height* 0.01),
                 child:
                 Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 24.0),
                      child: itemSlider,
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: arrayOfIcons.asMap().entries.map((entry) {
                          return GestureDetector(
                            onTap: () => _controller.animateToPage(entry.key),
                            child: Container(
                              width: 6.0,
                              height: 6.0,
                              margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: (Theme.of(context).brightness == Brightness.dark
                                      ? Colors.white
                                      : Colors.black)
                                  .withOpacity(_current == entry.key ? 0.9 : 0.4)),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
              );  
            },
          );
        }).toList(),
      )
    );
  }
}