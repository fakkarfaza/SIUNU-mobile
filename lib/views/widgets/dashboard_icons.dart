part of '../widgets.dart';



class DashboardIcons extends StatelessWidget {
  const DashboardIcons({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      height: 170,
      width: 600,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow:[
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3),
          )
        ] 
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18),
        child: 
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    children: [
                      IconButton(
                        onPressed: (){ 
                          Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => AkademikDashboard())
                          ); 
                        }, 
                        icon: Icon(Icons.school),
                      ),
                      InterFont.custom(text: 'Akademik', fontSize: 14, fontWeight: FontWeight.w600, color: Colors.black),
                    ],
                  ),
                ),
                const SizedBox(width: 6), 
                Expanded(
                  child: Column(
                    children: [
                      IconButton(
                        onPressed: (){ 
                          Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => PpmDashboard())
                          ); 
                        }, 
                        icon: Icon(Icons.person_search_outlined),
                      ),
                      InterFont.custom(text: 'PPM', fontSize: 14, fontWeight: FontWeight.w600, color: Colors.black),
                    ],
                  ),
                ),
                SizedBox(width: 6), 
                Expanded(
                  child: Column(
                    children: [
                      IconButton(
                        onPressed: () async { 
                          final url = 'https://cashplus.id/';
                           await launchUrl(Uri.parse(url));
                        }, 
                        icon: Image.asset('assets/images/cashplus_icon.png', height: 32),
                      ),
                      InterFont.custom(text: 'CashPlus', fontSize: 14, fontWeight: FontWeight.w600, color: Colors.black),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    children: [
                      IconButton(
                        onPressed: (){ 
                          Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => OperasionalDashboard())
                          ); 
                        }, 
                        icon: Icon(Icons.settings_outlined),
                      ),
                      InterFont.custom(text: 'Operasional', fontSize: 14, fontWeight: FontWeight.w600, color: Colors.black),
                    ],
                  ),
                ),
                SizedBox(width: 6), 
                Expanded(
                  child: Column(
                    children: [
                      IconButton(
                        onPressed: (){ 
                          Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => MbkmDashboard())
                          ); 
                        },  
                        icon: Icon(Icons.school),
                      ),
                      InterFont.custom(text: 'MBKM', fontSize: 14, fontWeight: FontWeight.w600, color: Colors.black),
                    ],
                  ),
                ),
                SizedBox(width: 6), 
                Expanded(
                  child: Column(
                    children: [
                      IconButton(
                        onPressed: () async { 
                          final url = 'https://www.simakpay.id/';
                           await launchUrl(Uri.parse(url));
                        }, 
                        icon: Image.asset('assets/images/simakpay_icon.png', height: 32),
                      ),
                      InterFont.custom(text: 'SimakPay', fontSize: 14, fontWeight: FontWeight.w600, color: Colors.black),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}