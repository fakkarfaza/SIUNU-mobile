part of '../widgets.dart';


class PortalIcons extends StatelessWidget {
  const PortalIcons({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      height: MediaQuery.of(context).size.height * 0.15,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow:[
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3),
          )
        ] 
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32),
        child: 
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    children: [
                      IconButton(
                        onPressed: (){ 
                          Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => PmbPage())
                          ); 
                        }, 
                        icon: Icon(Icons.school),
                        color: Colors.green,
                      ),
                      InterFont.custom(text: 'Penerimaan Mahasiswa Baru', fontSize: 14, fontWeight: FontWeight.w600, color: Colors.black),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}