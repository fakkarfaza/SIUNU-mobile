part of '../widgets.dart';

class NewsCard extends StatelessWidget {
  final String description;
  final String imageUrl;
  final String imagePath;

  const NewsCard({
    Key? key,
    required this.description,
    required this.imageUrl,
    required this.imagePath,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 16),
      width: 250,
      height: 240,
      child: Card(
        color: Colors.white,
        elevation: 4, 
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10), 
        ),
        child: InkWell(
          onTap: () async { 
            await launchUrl(Uri.parse(imageUrl));
          }, 
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Center( // GAMBAR
                child: ClipRRect(
                  borderRadius: const BorderRadius.vertical(top: Radius.circular(8)),
                  child: Image.asset(
                    imagePath,
                    height: 120,
                    width: 250, 
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                height: 75,
                child: Padding( // TEKS BERITA
                  padding: const EdgeInsets.only(left: 16, top: 11, right: 16),
                  child: InterFont.custom(text: description, fontSize: 14, fontWeight: FontWeight.w800, color: Colors.black, textAlign: TextAlign.start, maxLines: 3, overflow: TextOverflow.ellipsis),
                ),
              ),
              Padding( // FOOTER
                padding: const EdgeInsets.only(left: 16, top: 8, right: 16),
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: InterFont.custom(text: 'POLBAN', fontSize: 12, fontWeight: FontWeight.w400, color: Colors.black, textAlign: TextAlign.start),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}