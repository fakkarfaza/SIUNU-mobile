part of '../widgets.dart';

class MyButton extends StatelessWidget {
  
  final Function()? onTap;
  
  const MyButton({super.key, required this.onTap});

  

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 48,
        padding: const EdgeInsets.all(12.0), // Sesuaikan dengan kebutuhan Anda
        margin: const EdgeInsets.symmetric(horizontal: 25.0),
        decoration: BoxDecoration(
          color: Colors.green,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Center(
          child: InterFont.custom(text: 'Login', fontSize: 12, fontWeight: FontWeight.w600, color: Colors.white, overflow: TextOverflow.ellipsis)
        ),
      ),
    );
  }
}