part of '../widgets.dart';

class AkademikDrawer extends StatelessWidget {
  const AkademikDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
           SizedBox(
            height: 120,
            child: DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.green,
              ),
              child: InterFont.custom(text: 'Akademik', fontSize: 32, fontWeight: FontWeight.w800, color: Colors.white)
            ),
           ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.grey),
            title: InterFont.custom(text: 'Dashboard', fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  AkademikDashboard())); 
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20), 
            child: InterFont.custom(text: 'REKAP', fontSize: 12, fontWeight: FontWeight.w400, color: Color.fromARGB(255, 187, 187, 187)),
          ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.grey),
            title: InterFont.custom(text: 'Mahasiswa', fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  AkademikMahasiswa())); 
            },
          ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.grey),
            title: InterFont.custom(text: 'Dosen', fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  AkademikDosen())); 
            },
          ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.grey),
            title: InterFont.custom(text: 'Nilai IP/IPK', fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  AkademikNilai())); 
            },
          ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.grey),
            title: InterFont.custom(text: 'Kelulusan', fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  AkademikKelulusan())); 
            },
          ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.grey),
            title: InterFont.custom(text: 'Tracer Study', fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  AkademikTracerStudy())); 
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20), 
            child: InterFont.custom(text: 'LAIN-LAIN', fontSize: 12, fontWeight: FontWeight.w400, color: Color.fromARGB(255, 187, 187, 187)),
          ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.grey),
            title: InterFont.custom(text: 'Kalender Akademik', fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  AkademikKalender())); 
            },
          ),
        ],
      ),
    );
  }
}