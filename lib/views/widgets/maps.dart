part of '../widgets.dart';

class Maps extends StatefulWidget {
  const Maps({super.key});

  @override
  State<Maps> createState() => _MapsState();
}

class _MapsState extends State<Maps> {
  @override
  Widget build(BuildContext context) {
    return FlutterMap(
    options: const MapOptions(
      initialCenter: LatLng(-7.423570, 112.706390),
      initialZoom: 9.2,
      interactionOptions: InteractionOptions(
        enableMultiFingerGestureRace: true,
        flags: InteractiveFlag.doubleTapDragZoom |
          InteractiveFlag.doubleTapZoom |
          InteractiveFlag.drag |
          InteractiveFlag.flingAnimation |
          InteractiveFlag.pinchZoom |
          InteractiveFlag.scrollWheelZoom,
      )
    ),
    children: [
      TileLayer(
        urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
        userAgentPackageName: 'com.example.app',
      ),
      RichAttributionWidget(
        attributions: [
          TextSourceAttribution(
            'OpenStreetMap contributors',
            onTap: () => launchUrl(Uri.parse('https://openstreetmap.org/copyright')),
          ),
        ],
      ),
      MarkerLayer(
        markers: [
          Marker(
            point: const LatLng(-7.423570, 112.706390),
            width: 20,
            height: 20,
            child: Image.asset('assets/images/map_pin.png'),
          )
        ]
      )
    ],
  );
  }
}