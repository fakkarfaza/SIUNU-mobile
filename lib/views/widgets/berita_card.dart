part of '../widgets.dart';

class BeritaCard extends StatelessWidget {
  const BeritaCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Ink(
        height: 240,
          width: 250,
          decoration: 
            BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: const Color(0xFFF8F9FE),
              boxShadow:[
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 10,
                  offset: Offset(0, 3),
                )
              ] 
            ),
        child: InkWell( 
          onTap: () async { 
            final url = 'https://lptnu.or.id/2024/02/09/ketua-umum-pbnu-gus-yahya-apresiasi-langkah-menarik-universitas-nahdlatul-ulama-pasuruan/';
            await launchUrl(Uri.parse(url));
            }, // Isi Link berita
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(topLeft: Radius.circular(8), topRight: Radius.circular(8)),
                  child: Image.asset(
                    'assets/images/wisuda.jpg',
                    height: 120,
                    width: 250,
                    fit: BoxFit.cover
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16, top: 11, right: 16),
                child: 
                  InterFont.custom(text: 'Ketua Umum PBNU, Gus Yahya, Apresiasi Langkah Menarik Universitas Nahdlatul Ulama Pasuruan', fontSize: 14, fontWeight: FontWeight.w800, color: Colors.black, textAlign: TextAlign.start, maxLines: 4, overflow: TextOverflow.ellipsis),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16, bottom: 11, right: 16),
                child: Expanded(
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child:
                      InterFont.custom(text: 'POLBAN', fontSize: 12, fontWeight: FontWeight.w400, color: Colors.black, textAlign: TextAlign.start),
                  ) 
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}