part of '../widgets.dart';



class StatisticCard extends StatelessWidget {
  final IconData icon;
  final Color iconColor;
  final String data;
  final String title;
  final double? size;

  const StatisticCard({
    Key? key,
    required this.icon,
    required this.iconColor,
    required this.data,
    required this.title,
    this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 140,
      width: size,
      decoration: BoxDecoration(
        boxShadow:[
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            blurRadius: 18,
            offset: Offset(0, 4),
          )
        ], 
        borderRadius: BorderRadius.circular(6.0),
        color: Colors.white,
      ),
      child: Column(
        children: [
          const SizedBox(height: 20),
          CircleAvatar(
            backgroundColor: iconColor.withOpacity(0.16),
            radius: 20,
            child: Icon(
              icon,
              color: iconColor,
            ),
          ),
          SizedBox(height: 8),
          InterFont.custom(text: data, fontSize: 18, fontWeight: FontWeight.w500, color: Color(0xFF4B465C)),
          Padding(
            padding: const EdgeInsets.fromLTRB(4, 2, 4, 4),
            child:
          InterFont.custom(text: title, fontSize: 13, fontWeight: FontWeight.w400, color: Color(0xFF4B465C), textAlign: TextAlign.center),  
          ),
        ],
      ),
    );
  }
}