part of '../widgets.dart';

class IconWidget extends StatelessWidget {
  final dynamic icon;
  final String title;
  final Widget? page;
  final String? url;

  const IconWidget({
    Key? key,
    required this.icon,
    required this.title,
    this.page,
    this.url,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          IconButton(
            onPressed: () async {
              if (page != null && url == null) {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => page!)); 
              }
              else if (page == null && url != null) {
                await launchUrl(Uri.parse(url!));
              }
              else {
                throw Exception('Redirect error');
              }
            },
            icon: icon is IconData // Periksa apakah icon adalah IconData
              ? Icon(
                  icon,
                  color: Colors.green,
                  size: 32,
                )
              : icon is String // Periksa apakah icon adalah String
                  ? Image.asset(
                      icon,
                      height: 32,
                    )
                  : throw ArgumentError('Invalid icon type. Must be IconData or String.'),
          ),
          InterFont.custom(text: title, fontSize: 14, fontWeight: FontWeight.w600, color: Colors.black),
        ],
      ),
    );
  }
}
