part of '../widgets.dart';

class PPMDrawer extends StatelessWidget {
  const PPMDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
           SizedBox(
            height: 120,
            child: DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.green,
              ),
              child: InterFont.custom(text: 'P2M', fontSize: 32, fontWeight: FontWeight.w800, color: Colors.white)
            ),
           ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.grey),
            title: InterFont.custom(text: 'Dashboard', fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  PpmDashboard())); 
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20), 
            child: InterFont.custom(text: 'Rekap', fontSize: 12, fontWeight: FontWeight.w400, color: Color.fromARGB(255, 187, 187, 187)),
          ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.grey),
            title: InterFont.custom(text: 'Penelitian', fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  PpmPenelitian())); 
            },
          ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.grey),
            title: InterFont.custom(text: 'HKI (Hak Kekayaan Intelektual)', fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  PpmHki())); 
            },
          ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.grey),
            title: InterFont.custom(text: 'Hibah', fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  PpmHibah())); 
            },
          ),
        ],
      ),
    );
  }
}