import "package:flutter/material.dart";

import "package:animated_splash_screen/animated_splash_screen.dart";
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_map/flutter_map.dart';
import "package:google_fonts/google_fonts.dart";
import 'package:latlong2/latlong.dart';
import 'package:ptnu_mobile_app/utils/inter_font.dart';
import 'package:ptnu_mobile_app/utils/webview.dart';
import 'package:ptnu_mobile_app/utils/browser.dart';
import "package:ptnu_mobile_app/views/pages.dart";
import "package:url_launcher/url_launcher.dart";




part 'widgets/my_button.dart';
part 'widgets/my_textfield.dart';
part 'widgets/password_textfield.dart';
part 'widgets/dashboard_icons.dart';
part 'widgets/dashboard_swipes_icons.dart';
part 'widgets/icon_widget.dart';
part 'widgets/portal_icons.dart';
// part 'widgets/dashboard_icones.dart';
part 'widgets/berita_card.dart';
part 'widgets/statistic_card.dart';
part 'widgets/news_card.dart';
part 'widgets/maps.dart';
part 'widgets/footer.dart';
part 'widgets/akademik_drawer.dart';
part 'widgets/ppm_drawer.dart';
