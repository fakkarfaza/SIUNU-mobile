import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:ptnu_mobile_app/utils/const.dart';


class AuthService {
  static Future<http.Response> signIn(String number, String password) async {
    final storage = FlutterSecureStorage();
    String apiURL = BASE_URL + '/login'; //Api Staging

    Map<String, dynamic> requestBody = {
      'number': number,
      'password': password,
    };

    try {
      http.Response response = await http.post(
        Uri.parse(apiURL),
        headers: {
          'Content-Type' : 'application/json',
          'Accept' : 'application/json',
        },
        body: jsonEncode(requestBody),
      );
      if (response.statusCode == 200) {
        return response;
      } else {
        throw Exception(json.decode(response.body)['message']);
      }
    } catch (e) {
      throw '$e';
    }
  }
}

