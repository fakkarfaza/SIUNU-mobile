import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';

class InterFont {
  static custom({
    required text,
    required double fontSize,
    required FontWeight fontWeight,
    required Color color,
    TextAlign? textAlign,
    TextOverflow? overflow,
    int? maxLines,
    TextDecoration? decoration,
    
  }) {
    return Text (
      text,
      style: GoogleFonts.inter(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: color,
        decoration: decoration,
      ),
      overflow: overflow,
      maxLines: maxLines,
      textAlign: textAlign,
    );
  }
}