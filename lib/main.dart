
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:ptnu_mobile_app/utils/browser.dart';
import 'package:ptnu_mobile_app/views/pages.dart';
import 'dart:io';


class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext? context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}

void main() {
  HttpOverrides.global = MyHttpOverrides();
  runApp(const MyApp());
  // runApp(
  //   MaterialApp(
  //     home: Browser(),
  //     debugShowCheckedModeBanner: false,
  //     theme: ThemeData.dark(),
  //   )
  // );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashPage(), 
    );
  }

}

class CheckAuth extends StatefulWidget {
  const CheckAuth({Key? key}):super (key: key);

  @override
  State<CheckAuth> createState() => _CheckAuthState();
}

class _CheckAuthState extends State<CheckAuth> {

  bool isAuth = false;

  @override
  void initState() {
    super.initState();
    _checkIfLoggedIn();
  }

  void _checkIfLoggedIn() async {
    String? token = await FlutterSecureStorage().read(key: 'token');
      if(token != null && token.isNotEmpty){
        setState(() {
        isAuth = true;
      });
    } else {
      print('Tidak ada token');
    }
  }


  @override
  Widget build(BuildContext context) {
    Widget child;
    if(isAuth){
      print('token');
      child = DashboardPage();
    }else{
      child = PortalPage();
    }
    return Scaffold(
      body: child,
    );
  }
}